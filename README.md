pasp V0.9
=========

(c) 2016-2017 Éric Würbel, LSIS-CNRS UMR7196
<eric.wurbel@lsis.org>

pasp is an application which implements Possibilistic Answer Set
Programming. The semantics are those found in Bauters & al. [1]

It comes with two prolog programs : `pasp` encodes the possibilistic
normal logic program given as input into a normal logic program, which
can be transmitted to an ASP grounder and solver (eg clingo,
gringo+clasp, dlv, lparse+smodels...)

The program `paspfilter` takes as input the output of a clingo solver
and outputs the possibilistic stable models of the original program.

Note that at this time, only normal or extended logic programs are
handled. There are plans to implement the handling of disjunctive
programs.

See the documentation in =doc= folder for further instructions.

Bibliography
============

- [1] :: Kim Bauters and Steven Schockaert and Martine De Cock and
  Dirk Vermeir, "Characterizing and Extending Answer Set Semantics
  using Possibility Theory", Theory and Practice of Logic Programming,
  15(1), pp79-116, 2013

