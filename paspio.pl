%% -*- prolog -*-
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%
%% Copyright 2016 Éric Würbel, LSIS-CNRS, AMU
%%
%% This file is part of PASP, a possibilistic ASP solver.
%% PASP is free software: you can redistribute it and/or
%% modify it under the terms of the GNU General Public License as
%% published by the Free Software Foundation, either version 3 of
%% the License, or (at your option) any later version.
%%
%% PASP is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
%% General Public License for more details.
%%
%% You should have received a copy of the GNU General Public
%% License along with PASP. If not, see
%% <http://www.gnu.org/licenses/>.
%%
%% PASP implements the simulation of possibilistic ASP with
%% a classical ASP solver.
%%
%% This module defines predicates for handling the loading of
%% programs.
%%
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


:-module(paspio, [
	     load_pasp_programs/2,
	     load_pasp_program/2,
	     write_clauses/2,
	     write_conjunct/2,
	     write_answer_sets/2
	 ]).

:-use_module(utils).
:-use_module(logic, [conjoin/3]).
:-use_module(asp).


%%	load_pasp_programs(+Filenames, -Wffs)
%
%	Load pasp programs.

load_pasp_programs([],true).
load_pasp_programs([FileName|FL],Wffs) :-
	load_pasp_program(FileName,Wff),
	load_pasp_programs(FL,Wffs1),
	conjoin(Wff,Wffs1,Wffs)
	.

%%	load_pasp_program(+Filename, -Wff)
%
%	Load a PASP program

load_pasp_program(Filename, Wff) :-
	exists_file(Filename),
	open(Filename,read,Stream),
	read_wff_loop(Stream,Wff)
	.
load_pasp_program(Filename, _) :-
	\+ exists_file(Filename),
	error('~w: not found~n', [Filename])
	.

read_wff_loop(Stream,Wff) :-
        read_term(Stream,Wff1,[module(asp)]),
        (Wff1 == end_of_file ->
                   Wff = true;
         %true               ->
            read_wff_loop(Stream,Wff2),
            conjoin(Wff1,Wff2,Wff))
        .

write_clauses(Conjunct, File) :-
        open(File,write,Stream),
        write_conjunct(Conjunct,Stream),
        close(Stream)
        .

write_conjunct((A, B),Stream) :-
        !,
        write_conjunct(A,Stream),
        write_conjunct(B,Stream)
        .
write_conjunct(R,Stream) :-
        write_term(Stream,R,[max_depth(0),numbervars(true),portray(true),module(asp)]),
        put(Stream,'.'),
        nl(Stream)
        .


%%	write_answer_sets(+Stream, +ASL)
%
%	Write the answer sets list ASL on Stream.

write_answer_sets(_,[]).
write_answer_sets(Stream,[as(N,AS)|L]) :-
	format(Stream,'Answer: ~10r~n',[N]),
	write_as(Stream,AS),
	nl(Stream),
	write_answer_sets(Stream,L)
	.

write_as(_,[]).
write_as(Stream,[At|L]) :-
	write_term(Stream,At,[max_depth(0),portray(true)]),
	put(Stream,' '),
	write_as(Stream,L)
	.



