%% -*- prolog -*-
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%
%% Copyright 2016 Éric Würbel, LSIS-CNRS, AMU
%%
%% This file is part of PASP, a possibilistic ASP solver.
%% PASP is free software: you can redistribute it and/or
%% modify it under the terms of the GNU General Public License as
%% published by the Free Software Foundation, either version 3 of
%% the License, or (at your option) any qlater version.
%%
%% PASP is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
%% General Public License for more details.
%%
%% You should have received a copy of the GNU General Public
%% License along with PASP. If not, see
%% <http://www.gnu.org/licenses/>.
%%
%% PASP implements the simulation of possibilistic ASP with
%% a classical ASP solver.
%%
%% This module defines predicates for handling the filtering of the
%% models of the resulting ASP simulation program.
%%
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

:-module(postfilter, [
	     collect_results/2,
	     filter_answer_sets/2
	 ]).

:-use_module(utils,[chars_codes/2]).

:-use_module(library(readutil)).
:-use_module(library(charsio)).

%%	collect_results(+Stream, -Results)
%
%	Reads, analyzes and collect results from clasp solver.

collect_results(Stream,Results) :-
	read_line_to_codes(Stream,Line),
	chars_codes(LineC,Line),
	(   Line = end_of_file ->
	    Results = [];
	    % ->
	    parse_result_line(LineC,R),
	    (	R = end ->
	        Results = [];
	        % true
	        (   R = garbage ->
		    collect_results(Stream, Results);
		    % true
	            Results = [R|Results1],
	            collect_results(Stream,Results1)
		)
	    )
	)
	.


%%	filter_answer_sets(+ASL,-FASL)
%
%	Filters a list of answer sets.

filter_answer_sets([],[]).
filter_answer_sets([answer_num(N),as(AS)|ASL],[as(N,FAS)|FASL]) :-
	filter_as(AS,FAS),
	filter_answer_sets(ASL,FASL)
       .

%%	filter_as(+AS,-FAS)
%
%	Filters an answer set. The filtering process is as follows : for
%	each atoms in the model which differ only on the necessity
%	degree, keep the one with the greatest necessity degree.

filter_as([],[]).
filter_as([A|L1],[MaxA|L2]) :-
	collect_identical_atoms(A,L1,LID,LRest),
	max_member(MaxA,[A|LID]),
	filter_as(LRest,L2)
	.

%%	collect_identical_atoms(+At,?InList,?IDList,?RestList)
%
%	InList is a list of ASP atoms (prolog terms).
%	IDList and RestList is a partition of Inlist. IDlist contains
%	atoms similar to At : same functor, same arity, same arguments
%	except the last one which is always a number (thus the arity of
%	terms is at least 1).

collect_identical_atoms(_,[],[],[]).
collect_identical_atoms(At,[A|L1],[A|L2],L3) :-
	At =.. [F|Args1],
	A =.. [F|Args2],
	length(Args1,Lg),
	length(Args2,Lg),
	eq_except_last(Args1,Args2),
	!,
	collect_identical_atoms(At,L1,L2,L3)
	.
collect_identical_atoms(At,[A|L1],L2,[A|L3]) :-
	collect_identical_atoms(At,L1,L2,L3)
	.

%%	eq_except_last(?L1,?L2)
%
%	L1 and L2 are identical, i.E. their elements are equal in the
%	sense of ==, except their last element.

eq_except_last([E1],[E2]) :- E1 \== E2.
eq_except_last([E1|L1],[E2|L2]) :-
	E1 == E2,
	eq_except_last(L1,L2)
	.


%%	Answer set solver results parser.

parse_result_line(Line,R) :-
	phrase(result_line(R),Line,_)
	.

result_line(garbage) -->
	['c','l','i','n','g','o'], space ,['v','e','r','s','i','o','n'], space,
	versionspec(_)
	.
result_line(garbage) -->
	['R','e','a','d','i','n','g'], space, ['f','r','o','m'], space,
	dirspec(_)
	.
result_line(garbage) -->
	['S','o','l','v','i','n','g','.','.','.']
	.
result_line(answer_num(Num)) -->
	answernum(Num)
	.
result_line(optimization(Num)) -->
	optimize(Num)
	.
result_line(end) -->
	['O','P','T','I','M','U','M',' ','F','O','U','N','D']
	.
result_line(end) -->
	['S','A','T','I','S','F','I','A','B','L','E']
	.
result_line(end) -->
	['U','N','S','A','T','I','S','F','I','A','B','L','E']
	.
result_line(as(AS)) -->
	answer_set(AS)
	.

%%	version specification (unused for now)

versionspec([V|VS]) -->
	number(V),
	subversion(VS).

subversion([V|VS]) -->
	['.'],
	number(V),
	subversion(VS)
	.
subversion([]) -->
	[]
	.

dirspec([C|DS]) -->
	[C],
	{\+ char_type(C, space)},
	dirspec(DS)
	.
dirspec([]) -->
	[]
	.

%%	Interpret the "Answer:" lines of clingo output.
answernum(Num) -->
	['A','n','s','w','e','r',':'], space, number(Num)
	.

%%	Interpret the "Optimization" lines of clingo output.
optimize(Num) -->
	['O','p','t','i','m','i','z','a','t','i','o','n',':'], space, number(Num)
	.

%%	collect answer sets from clingo output
answer_set([T|L]) -->
	term(LT),
	{
	 LT \= [],
	 chars_to_term(LT,T)
	},
	end_answer_set(L)
	.

end_answer_set([T|L]) -->
	[' '],
	term(LT),
	{
	    LT \= [],
	    chars_to_term(LT, T)
	},
	end_answer_set(L)
	.
end_answer_set([]) -->
	[' ']
	.
end_answer_set([]) -->
	[]
	.

term([C|L]) -->
	[C],
	{\+ char_type(C,space)},
	term(L)
	.
term([]) -->
	[]
	.


space -->
	[' '], space
	.
space -->
	[]
	.

number(N) -->
	digit(D0),
	digits(D),
	{ number_chars(N, [D0|D]) }
	.

digits([D|T]) -->
	digit(D),
	digits(T).
digits([]) --> [].

digit(D) -->
	[D],
	{ char_type(D,digit) }
	.



chars_to_term(Chars,Term) :-
	append(Chars,['.'],Chars1),
	open_chars_stream(Chars1,S),
	read(S,Term),
	close(S)
	.
















