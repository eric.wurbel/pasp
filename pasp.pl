%% -*- prolog -*-
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%
%% Copyright 2016 Éric Würbel, LSIS-CNRS, AMU
%%
%% This file is part of PASP, a possibilistic ASP solver.
%% PASP is free software: you can redistribute it and/or
%% modify it under the terms of the GNU General Public License as
%% published by the Free Software Foundation, either version 3 of
%% the License, or (at your option) any qlater version.
%%
%% PASP is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
%% General Public License for more details.
%%
%% You should have received a copy of the GNU General Public
%% License along with PASP. If not, see
%% <http://www.gnu.org/licenses/>.
%%
%% PASP implements the simulation of possibilistic ASP with
%% a classical ASP solver.
%%
%% This file contains the main predicates.
%%
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%	 written for swi-prolog

%%	Command line specification.

:-use_module(utils).
:-use_module(paspio).
:-use_module(generator).


go :-
	current_prolog_flag(argv, InputFiles),
	(   InputFiles = []
	->  error("no input files")
	;   true
	),
	load_pasp_programs(InputFiles,Wff),
	certp(Wff,Certp),
	pasp_to_asp(Wff,Certp,ASP),
	write_conjunct(ASP,user_output)
	.


test(Files) :-
	load_pasp_programs(Files,Wff),
	certp(Wff,Certp),
	pasp_to_asp(Wff,Certp,ASP),
	write_conjunct(ASP,user_output)
	.





