%% -*- prolog -*-
%%	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%
%%	Copyright 2016 Éric Würbel, LSIS-CNRS, AMU
%%
%%	This file is part of pasp. pasp is free
%%	software: you can redistribute it and/or modify it under the
%%	terms of the GNU General Public License as published by the Free
%%	Software Foundation, either version 3 of the License, or (at
%%	your option) any later version.
%%
%%	pasp is distributed in the hope that it will be useful,
%%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
%%	General Public License for more details.
%%
%%	You should have received a copy of the GNU General Public
%%	License along with PLRsf-solver. If not, see
%%	<http://www.gnu.org/licenses/>.
%%
%%	pasp implements possibilistic answer set programming.
%%
%%	This file is the initialization file of the pasp standalone
%%	executable.
%%
%%	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

:-ensure_loaded('pasp.pl').

